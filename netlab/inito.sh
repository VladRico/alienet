#!/bin/sh

set +u

RFSUUID=`awk '$2 == "/" {split($1,a,"="); print(a[2])}' /etc/fstab`
BOOTUUID=`awk '$2 == "/boot" {split($1,a,"="); print(a[2])}' /etc/fstab`
RFSDEV=`blkid --uuid "$RFSUUID"`
BOOTDEV=`blkid --uuid "$BOOTUUID"`

set -ue -o pipefail

init_overlay () {
        modprobe overlay
        mkdir /mnt/lower
        mount -t ext4 -o ro "$RFSDEV" /mnt/lower
        mkdir /mnt/rw
        mount -t tmpfs tmpfsrw /mnt/rw
        mkdir /mnt/rw/upper
        mkdir /mnt/rw/work
        mkdir /mnt/newrfs
        mount -t overlay -o lowerdir=/mnt/lower,upperdir=/mnt/rw/upper,workdir=/mnt/rw/work overlayrfs /mnt/newrfs
        mkdir /mnt/newrfs/ro
        mkdir /mnt/newrfs/rw
        grep -v "$RFSUUID" /mnt/lower/etc/fstab > /mnt/newrfs/etc/fstab
        cd /mnt/newrfs
        pivot_root . mnt
        exec chroot . sh -c "$(cat <<END
mount --move /mnt/mnt/lower/ /ro
mount --move /mnt/mnt/rw /rw
umount -l /mnt/mnt/boot
umount -l /mnt/mnt
umount -l /mnt/proc
umount -l /mnt/dev
umount -l /mnt
exec /sbin/init
END
)"
}


mount -t tmpfs initmpfs /mnt
mkdir /mnt/boot
mount -t ext4 -o ro "$BOOTDEV" /mnt/boot
if [ -f /mnt/boot/overlay ] ; then
        echo "overlay boot"
        init_overlay
else
        echo "normal boot"
        umount /mnt/boot
        umount /mnt
        exec /sbin/init
fi
