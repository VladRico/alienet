# Netlab
## mount /etc as 9p fs
- Add to guest `/etc/fstab`:
```
9p_etc  /etc  9p  trans=virtio,version=9p2000.L 0 0
```

- Save your current guest `/etc` to a directory on the host `/home/user/path/<vmname>/etc`
```
sudo modprobe nbd
sudo qemu-nbd --connect /dev/nbd0 ./lab-template.qcow2
sudo mount /dev/nbd0p3 /mnt/lab
sudo rsync -av /mnt/lab/etc /home/user/path/<vmname>
sudo umount /mnt/lab
sudo qemu-nbd -d /dev/nbd0
sudo modprobe -r nbd
```

- Change the user to avoid permissions issues with the guest
`sudo chown -R libvirt-qemu:libvirt-qemu /home/user/path/<vmname>/etc`

- Add 9p fs `sudo virsh edit <vmname>` in `<devices></devices>`
```xml
<filesystem type="mount" accessmode="mapped">
  <source dir="/home/<user>/path/<vmname>/etc"/>
  <target dir="9p_etc"/>
  <address type="pci" domain="0x0000" bus="0x07" slot="0x00" function="0x0"/>
</filesystem>
```
- Boot



## Ro rfs on Alpine with overlayfs (wip)
- Start your alpine image and create `inito.sh` in `/` , then `chmod u+x /inito.sh`
- Enable overlay with `touch /boot/overlay`
- Edit `/etc/update-extlinux.conf` and add:
```
default_kernel_opts="rootfstype=ext4 init=/inito.sh"
# Not necessary but useful for debugging
verbose=1
serial_port=ttyS0
```
- Run `update-extlinux` to apply new configuration
- to disable ro rfs , `rm /boot/overlay` 
