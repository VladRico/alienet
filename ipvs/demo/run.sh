#!/bin/bash

usage() {
	echo "This script must be run as root"
	echo -e "\nUsage: $0 ipvsAddress nbTorInstance\n"
}

trap "ipvsadm -C; docker-compose stop; exit 0;" 2 3

# Show help
if [[ "$*" == "--help" || "$*" == "-h" ]] 
then
	usage
	exit 0
fi

# Check if user is root
if [[ "$EUID" -ne 0 ]]
then
	echo "This script must be run as root"
	exit 1
fi

# Check if $1 is a valid ip address
if [[ ! -z $1 ]]
then
	regexp='([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])'

	if [[ "$1" =~ ^$regexp\.$regexp\.$regexp\.$regexp$ ]]
	then
		# Create ipvs addr TCP , UDP and round robin mode
		echo "Using $1"
		ipvsadm -A -t $1:9050 -s rr
		ipvsadm -A -u $1:9050 -s rr
	else
		echo "ipvs address is not valid: $1"
		echo "Using 1.2.3.4"
		ipvsadm -A -t 1.2.3.4:9050 -s rr
		ipvsadm -A -u 1.2.3.4:9050 -s rr

		# Assign default value to $1
		set -- "1.2.3.4" "${@:2}"	
	fi
else
	echo "Using 1.2.3.4"
	ipvsadm -A -t 1.2.3.4:9050 -s rr
	ipvsadm -A -u 1.2.3.4:9050 -s rr

	# Assign default value to $1
	set -- "1.2.3.4" "${@:2}"	
fi


# Run tor containers instance
if [[ -z "$2" ]] 
then
	echo -e "Running 3 tor instances\n"
	docker-compose up -d --scale tor=3
	set -- "${@:1}" "3"
else
	echo -e "Running $2 tor instances\n"
	docker-compose up -d --scale tor="$2"
fi


# Get containers ip address
ips=$(docker network inspect net_tor | jq -r 'map(.Containers[].IPv4Address) []' | rev | cut -c4- | rev)

# Add containers ip to ipvs
for ip in $(echo "$ips")
do
	#echo "$ip"
	ipvsadm -a -t $1:9050 -r "$ip":9050 -m
	ipvsadm -a -u $1:9050 -r "$ip":9050 -m
done

echo -e "\n"
ipvsadm -l 
echo -e "\nTry it with \n-->\tcurl -x socks5://$1:9050 https://checkip.amazonaws.com/"
echo -e "\nStop and clear with CTRL-C\n"


# Used to reset tor circuit each time a request is made
# --> Infinite pool of IPs (limit is the number of tor exits nodes)
# Uncomment from 91 to 101 and remove the sleep (line 102)

i=1
while true
do

	 mon=$(netstat -laputden | grep "$1:9050" | grep -v '0.0.0.0' )
	 if [[ ! -z "$mon" ]]
	 then
	 	docker exec demo-tor-$i kill -HUP 1
	 	i=$((i+1))

	 	if [[ $i -eq $2 ]]
	 	then
	 		i=1
	 	fi
	 fi
	#sleep 0.1

done
