# Wireguard To another NameSpace
## Play with linux wireguard Tunnel and send it in another namespace for fun and profits

### **Disclaimer**
Let's be clear, I'm not an absolute fan of Docker.  
Docker is an excellent tool for developers and allows me to quickly and easily check certain properties.  
**Docker is only used for this purpose in my labs**.  
Docker is not suitable for production (I assume what I say).  
Container orchestration in production is a real complex subject.


### **Docker, where is your container network namespace ? (the missing file reference)**
First, let's create our own network namespace **"testns"**.  
```bash
sudo ip netns add testns
```  
We can show "testns" in the netnwork namespace list and we can check that a specific "testns" entry is created in /var/run/netns directory:  
```bash
sudo ip netns ls
testns
$ ls /var/run/netns/
testns
```
Let's create a small alpine container and bind a shell in it:
```bash
docker run --rm -dit --name alp alpine /bin/ash
docker exec -it alp /bin/sh
# ip addr

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
So, we can see that 2 interfaces are living in the container (l0 and eth0@if16) with an ip addresse. This 2 interfaces are living in a dedicated network namespace.  
However, on the host side, we can not retrieve this namespace:  
```bash
sudo ip netns ls
testns
$ ls /var/run/netns/
testns
```
Only the old testns network namespace that we've created at the begining is shown.  
Docker do not use the system /var/run/netns entries and we have had it manually in order the container namespace being visible.  
Let's check the PID namespace of the container:  
```bash
docker inspect -f '{{.State.Pid}}' alp
18377
```
You can retrieve container namespace in system thanks to the proc PID with: 
```bash
sudo ls /proc/18377/ns
cgroup  ipc  mnt  net  pid  pid_for_children  time  time_for_children  user  uts
```
The network namespace is represented with the net entry.  
So that the system can also see and interact with this namespace, there's two way depends of the version of iproute2 binarie installed on your OS:  
- Latest iproute2 version allow you to directly attach the container namespace with the following commands:  
    ```bash
    sudo ip netns attach alp 18377
    sudo ip netns ls 
    testns
    alp
    ```
- Older iproute2 versions do not permitts the attach command and you have to bind mount directly in the /var/run/netns file system hierarchy:
    ```bash
    sudo touch /var/run/netns/alp
    sudo mount -o bind /proc/18377/ns/net /var/run/netns/alp
    sudo ip netns ls
    testns
    alp
    ```
whichever method you use, you can now check that the namespace is reachable and inspect its contents, where you will find the two interfaces and network addresses used in the alpine container.  
```bash
sudo ip netns exec alp ip addr

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

```
Ok, so now, we can play with the "alp" container network namespace.  
Let's delete the "testns" network namespace as we no longer need it:  
```bash
sudo ip netns del testns
sudo ip netns ls
alp
```
### **Use Wireguard demo server**
*Note: Wireguard is provided in Linux kernel >= 5.10*  
Wireguard guys had the good idea of providing a [demo server](https://www.wireguard.com/quickstart/#demo-server) so that we didn't have to set it up ourselves for our lab.  
There's an automatic script that lets you connect to this demo server and set your default route through the tunnel. The script is available [here](https://git.zx2c4.com/wireguard-tools/plain/contrib/ncat-client-server/client.sh).  
As this isn't exactly what we want to do, we'll use the information extracted from the script and build the tunnel ourselves by hand:
- First create the private and public key
    ```bash
    cd /tmp
    wg genkey | tee priv | wg pubkey > pub
    cat pub
    pplXx8CSLCbueZreteAaEnKpCTf0RHkxBglDVhF9FCI=
    ```
- Then, register to the demo server by sending our public key with netcat:
    ```bash
    nc demo.wireguard.com 42912
    pplXx8CSLCbueZreteAaEnKpCTf0RHkxBglDVhF9FCI=
    OK:JRI8Xc0zKP9kXk8qP84NdUQA04h6DLfFbwJn4g+/PFs=:12912:192.168.4.74
    ```
    The demo server responds Ok, gives it's public key "JRI8Xc0zKP9kXk8qP84NdUQA04h6DLfFbwJn4g+/PFs=", the port to join: "12912" and the private ip you have to use: "192.168.4.74"
- Finally, create the Tunnel
```bash
sudo ip link add wg0 type wireguard
sudo wg set wg0 private-key priv peer "JRI8Xc0zKP9kXk8qP84NdUQA04h6DLfFbwJn4g+/PFs=" allowed-ips 0.0.0.0/0 endpoint demo.wireguard.com:12912 persistent-keepalive 25
sudo ip addr add 192.168.4.74/24 dev wg0
sudo ip link set wg0 up
```
You can check wgo interface in your system with:
```bash
ip addr

...
wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000 link/none
inet 192.168.4.74/24 scope global wg0
       valid_lft forever preferred_lft forever

``` 
and test connectivity:  
```bash
ping -c2 192.168.4.1

PING 192.168.4.1 (192.168.4.1) 56(84) bytes of data.
64 bytes from 192.168.4.1: icmp_seq=1 ttl=64 time=102 ms
64 bytes from 192.168.4.1: icmp_seq=2 ttl=64 time=102 ms
```
Here we are, we have a functionnal wireguard Tunnel.  
You can also join the minimal http server hosted on [http://192.168.4.1](http://192.168.4.1) with your favorite browser.  
*Note: you can also join all the clients who are trying wireguard demo server in the 192.168.4.0/24 ipv4 range. Dont Be Evil !! (nmap -Pn 192.168.4.0/24)*  


### **Interesting Linux Tunnels Property**
There are many tunnel technologies available under Linux. These include ip6 over ip4, ipip tunnels, GRE tunnels, VXLAN etc...  
There are also tunnels enabling data encryption, such as the vti interfaces used with ipsec, or the wireguard interfaces we're interested in today.  
An interesting property of Linux tunnels is that they survive namespace relocation.
If a tunnel has been initiated in a given namespace, then it can continue to operate in another namespace that has no knowledge of the underlying interface on which the tunnel is based.  
Even if at first glance it seems surprising, it's nevertheless understandable. It's just like a physical interface. It can exist (and therefore be used) in different namespaces in your system.  
This property allows for interesting configurations and can, in some cases, greatly simplify problems, whether of a safety or technical nature.

### **Send your tunnel in docker namespace**
It's possible to send the "wg0" Tunnel interface in the previous created alp network namespace; it's as simple as that:
```bash
sudo ip link set wg0 netns alp
``` 
You can check that the wg0 interface do not belongs anymore to the native namespace but lives in the alp namespace
```bash
$ sudo ip netns exec alp ip link

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
7: wg0: <POINTOPOINT,NOARP> mtu 1420 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/none 
```
let's give some ip property for the wg0 inside the alp namespace (ip property are lost when mooving from another namespace):  
```bash
sudo ip netns exec alp ip addr add 192.168.4.74/24 dev wg0
sudo ip netns exec alp ip link set wg0 up
``` 
Check that the wg0 Tunnel is still functionnal inside alp container:  
```bash
$ docker exec -it alp /bin/sh

# ping -c2 192.168.4.1
PING 192.168.4.1 (192.168.4.1): 56 data bytes
64 bytes from 192.168.4.1: seq=0 ttl=64 time=102.257 ms
64 bytes from 192.168.4.1: seq=1 ttl=64 time=102.368 ms
--- 192.168.4.1 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 102.257/102.312/102.368 ms
```
That's it, the Tunnel has moved in another namespace and it's still functionnal (interesting to note that encapuslation / encryption / decryption is still done in native network namespace).
### Play with it !
you can do some really cool stuff with this property. For example, you could use a wireguard VPN to enable two (or more) containers located on differents machines or/and on differents networks to securely communicate whitout any complicated iptables / ip route tricks between hosts.
```
                     +-------------------------+
                     |                         |
                     |     Wireguard Server    |
    +---------------->                         <----------------+
    |                |                         |                |
    |                +-------------------------+                |
    |                                                           |
    |                                                           |
    |                                                           |
+---+------------------+                    +-------------------+--+
|   |           Host1  |                    |  Host 2           |  |
|   |                  |                    |                   |  |
|   +---------------+  |                    |   +---------------+  |
|   |               |  |                    |   |               |  |
|   |               |  |                    |   |               |  |
|   |  Container 1  |  |                    |   |  Container 2  |  |
|   |               |  |                    |   |               |  |
|   |               |  |                    |   |               |  |
|   +---------------+  |                    |   +---------------+  |
|                      |                    |                      |
+---------+------------+                    +----------+-----------+
          |                                            |
          |                                            |
  +-------+--------+                            +------+---------+
  |                |                            |                |
  |   Internet     |                            |     Internet   |
  |                |                            |                |
  +----------------+                            +----------------+
```
### **Real use cases**
In addition to the use case described above, and in a more serious vein, here are some concrete examples you can find on the web (whitout Docker usage) 
- Use specific netnwork name space for addressing default vpn route with wireguard: [https://www.wireguard.com/netns/#routing-network-namespace-integration](https://www.wireguard.com/netns/#routing-network-namespace-integration)
- Route-based IPsec VPN on Linux with strongSwan: [https://vincent.bernat.ch/en/blog/2017-route-based-vpn](https://vincent.bernat.ch/en/blog/2017-route-based-vpn)
- Route-based VPN on Linux with WireGuard: [https://vincent.bernat.ch/en/blog/2018-route-based-vpn-wireguard](https://vincent.bernat.ch/en/blog/2018-route-based-vpn-wireguard)

### **Come back to normality**
 
```bash
# Move wg0 Tunnel interface in native namespace
sudo ip netns exec alp ip link set wg0 netns 1
# Remove Tunnel
sudo ip link del wg0
# Unlink alp network namespace
sudo ip netns del alp
# Stop (and delete) the alpine container
docker stop alp
```