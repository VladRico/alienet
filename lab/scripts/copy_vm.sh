#!/bin/bash

if [[ $EUID != 0 ]]; then
  echo "Please run as root"
  exit
fi

set +u
IMGPATH="/home/vlad/Documents/kvm"
SOURCE_NAME="lab-template"
NEW_NAME="lab1337"
ORIGINAL_QCOW2="$SOURCE_NAME.qcow2"
XML=""
PARTITION="/dev/nbd0p3"
MOUNT_PATH="/dont/exist"
USER_CHOICE="BAD"

set -ue -o pipefail

cleanup(){
  echo -e "Umounting partition"
  [ -d "$MOUNT_PATH/dev" ] && umount "$MOUNT_PATH"/dev
  [ "$(mount | grep "$MOUNT_PATH")" ] && umount "$MOUNT_PATH"
  echo "Disconnecting nbd0"
  qemu-nbd -d /dev/nbd0 
  sleep 1 
  [[ "$(lsmod | grep nbd)" ]] && modprobe -r nbd
  echo "Exiting"

}

define_base(){
  read -p "New VM name [$NEW_NAME]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && NEW_NAME="$USER_CHOICE"

  read -p "Based on which one [$SOURCE_NAME]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && SOURCE_NAME="$USER_CHOICE"

  read -p "Path to qcow2 directory [$IMGPATH]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && IMGPATH="$USER_CHOICE"
  echo ""

}

shutdown_vm(){
if [ "$(virsh list --all | grep "$SOURCE_NAME" | awk '{print $3}')" == "running" ]
then
  echo -e "Shutdown $SOURCE_NAME"
  virsh shutdown --domain "$SOURCE_NAME"
fi 

}

choose_partition(){
  echo -e "Available partitions:"
  fdisk -l | grep '^/dev/nbd0p[0-9]'
  PARTITION="/dev/nbd0p3"
  read -p "Choose a partition [$PARTITION]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && PARTITION="$USER_CHOICE"
  
  ! [[ "$PARTITION" =~ ^/dev/nbd0p[0-9]$ ]] && choose_partition
  echo "$PARTITION"

}

choose_mount_path(){
  MOUNT_PATH="/mnt/lab"
  read -p "Path to mount $PARTITION [$MOUNT_PATH]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && MOUNT_PATH="$USER_CHOICE"

  [ ! -d "$MOUNT_PATH" ] && choose_mount_path
  echo "$MOUNT_PATH"

}

check_domain_exist(){
  if [ "$(virsh list --all | awk '{print $2}' | grep -E "^$NEW_NAME$")" ]
  then
    USER_CHOICE="yes"
    echo -e "The domain $NEW_NAME already exist"
    read -p "Do you want to delete it [yes]: " USER_CHOICE
    if [ "$USER_CHOICE" == "yes" ] || [ -z "$USER_CHOICE" ]
    then  
      virsh undefine --domain "$NEW_NAME"
    fi
  fi
}

trap "cleanup" INT EXIT

# Define variables
define_base

shutdown_vm

# Duplicate original disk
cp "$IMGPATH/$ORIGINAL_QCOW2" "$IMGPATH/$NEW_NAME.qcow2"

# Editing virsh XML data
XML=$(virsh dumpxml --domain "$SOURCE_NAME" |\
  sed -e "/<mac address=/d" |\
  sed -e "/<uuid/d" |\
  sed -e "s/<name>$SOURCE_NAME<\/name>/<name>$NEW_NAME<\/name>/" |\
  sed -e "s/$ORIGINAL_QCOW2/$NEW_NAME.qcow2/")

# Creating new VM
USER_CHOICE=""
check_domain_exist
if [ -z "$USER_CHOICE" ] || [ "$USER_CHOICE" == "yes" ]
then
  echo "$XML" | virsh define /dev/stdin
fi

echo -e "Verifying nbd module is loaded"
[ "$(lsmod | grep nbd)" ] || modprobe nbd

qemu-nbd --connect /dev/nbd0 "$IMGPATH/$NEW_NAME.qcow2"

choose_partition
choose_mount_path

echo -e "Mounting $PARTITION to $MOUNT_PATH"
mount "$PARTITION" "$MOUNT_PATH"

echo -e "Mounting /dev to $MOUNT_PATH/dev"
mount --bind /dev "$MOUNT_PATH"/dev # Needed for ssh-keygen -A
chroot "$MOUNT_PATH" /bin/bash << EOF

find /etc/ssh/ -regex ".*\(key\|pub\)$" -exec /bin/rm {} \;
ssh-keygen -A
echo "$NEW_NAME" > /etc/hostname

EOF

echo "$NEW_NAME successfully setup"
echo -e "\n == Cleaning ==\n"
# cleanup function executed
