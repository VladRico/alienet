#!/bin/bash

if [[ $EUID != 0 ]]; then
  echo "Please run as root"
  exit
fi

set +u
IMGPATH="/home/vlad/Documents/kvm"
SOURCE_NAME="lab-tpl-9p"
NEW_NAME="lab1337"
ORIGINAL_QCOW2="$SOURCE_NAME.qcow2"
XML=""
USER_CHOICE="BAD"

set -ue -o pipefail


define_base(){
  read -p "New VM name [$NEW_NAME]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && NEW_NAME="$USER_CHOICE"

  echo -e "Ensure that the template you choose has already the 9p fs defined in the virsh XML"
  read -p "Based on which one [$SOURCE_NAME]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && SOURCE_NAME="$USER_CHOICE"

  read -p "Path to qcow2 directory [$IMGPATH]: " USER_CHOICE
  [ -n "$USER_CHOICE" ] && IMGPATH="$USER_CHOICE"
  echo ""

}

shutdown_vm(){
if [ "$(virsh list --all | grep "$SOURCE_NAME" | awk '{print $3}')" == "running" ]
then
  echo -e "Shutdown $SOURCE_NAME"
  virsh shutdown --domain "$SOURCE_NAME"
fi 

}

check_domain_exist(){
  if [ "$(virsh list --all | awk '{print $2}' | grep -E "^$NEW_NAME$")" ]
  then
    USER_CHOICE="yes"
    echo -e "The domain $NEW_NAME already exist"
    echo "If yes, you'll need to remove the $NEW_NAME/etc on host by hand to avoid problems ;)"
    read -p "Do you want to delete it [yes]: " USER_CHOICE
    if [ "$USER_CHOICE" == "yes" ] || [ -z "$USER_CHOICE" ]
    then  
      virsh undefine --domain "$NEW_NAME"
    fi
  fi
}

#trap "cleanup" INT EXIT

# Define variables
define_base

shutdown_vm


# Editing virsh XML data
XML=$(virsh dumpxml --domain "$SOURCE_NAME" |\
  sed -e "/<mac address=/d" |\
  sed -e "/<uuid/d" |\
  sed -e "s/<name>$SOURCE_NAME<\/name>/<name>$NEW_NAME<\/name>/" |\
  sed -e "s|<source dir='$IMGPATH/$SOURCE_NAME/etc'|<source dir='$IMGPATH/$NEW_NAME/etc'|")

if ! [ "$(echo "$XML" | grep "filesystem")" ]
then
    echo -e "9p fs not found in virsh dumpxml $SOURCE_NAME"
    echo -e "Ensure that the template used contains the 9p fs definition like:"
    echo -e "<filesystem type='mount' accessmode='mapped'>
      <source dir='$SOURCE_NAME/etc'/>
      <target dir='9p_etc'/>
      <address type='pci' domain='0x0000' bus='0x07' slot='0x00' function='0x0'/>
    </filesystem>"
    echo -e "\n then run the script again"
    exit 1
fi

# Creating new VM
USER_CHOICE=""
check_domain_exist
if [ -z "$USER_CHOICE" ] || [ "$USER_CHOICE" == "yes" ]
then
  echo "$XML" | virsh define /dev/stdin
fi

# Duplicate /etc 
mkdir -p "$IMGPATH/$NEW_NAME"
rsync -av "$IMGPATH/$SOURCE_NAME/etc" "$IMGPATH/$NEW_NAME"

# Edit hostname
echo "$NEW_NAME" | tee "$IMGPATH/$NEW_NAME/etc/hostname"
# Delete old ssh key
find "$IMGPATH/$NEW_NAME/etc/ssh" -regex ".*\(key\|pub\)$" -exec /bin/rm {} \;

echo "$NEW_NAME successfully setup"
echo -e "\n == Cleaning ==\n"
# cleanup function executed
