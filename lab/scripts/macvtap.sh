#!/bin/bash
#

if [[ $EUID != 0 ]]; then
    echo "Please run as root"
    exit
fi

set +u

ACTION="setup"
IFNAME="enp6s0"
MACVLAN="macvlan1337"
IPADDR="192.168.3.9"

# Get netmask from interface
NETMASK=$(ip -o -f inet addr show $IFNAME | awk -F ' ' '{print $4}' | cut -d"/" -f2)
ORIGINAL_IP=$(ip -o -f inet addr show $IFNAME | awk -F ' ' '{print $4}' | cut -d"/" -f1)
# Get network (192.168.3.0/24) from given interface
NETWORK="$(ip -o -f inet addr show $IFNAME | awk -F ' ' '{print $4}' | cut -d"." -f1-3).0/$NETMASK"
ROUTE=""

set -ue -o pipefail

usage() {
  echo -e "Used to create a macvlan interface,\n"\
    "allowing communication between host and a guest that use a macvtap interface\n"\
    "by routing the traffic through the macvlan interface"
  echo ""
  echo "usage: $0 (-s|-c) -i <ifname> -n <macvlan> -4 <ipaddr>"
  echo ""
  echo "Must be run as root"
  echo ""

  echo -e "-s \t\t\t setup" 
  echo -e "-c \t\t\t cleanup"
  echo -e "-i <ifname> \t\t name of the host interface (Default: $IFNAME)"
  echo -e "-n <macvlan> \t\t name of the macvtap interface (Default: $MACVLAN)"
  echo -e "-4 <ipaddr> \t\t IPv4 addr to use (Default: $IPADDR)"
  echo ""
  echo -e "Example:\n\t $0 -s -i $IFNAME -n $MACVLAN -4 $IPADDR" 
  echo -e "\t $0 -c -i $IFNAME -n $MACVLAN -4 $IPADDR" 

}

# Parse options
while getopts "i:n:4:sch" opt; do
  case ${opt} in
    i ) IFNAME="$OPTARG";;
    n ) MACVLAN="$OPTARG" ;;
    4 ) IPADDR="$OPTARG" ;;
    s ) ACTION="setup" ;;
    c ) ACTION="cleanup" ;;
    h ) usage && exit 0 ;;
    \? )
      echo "Invalid option: -$OPTARG" 1>&2
      exit 1
      ;;
    : )
      echo "Option -$OPTARG requires an argument" 1>&2
      exit 1
      ;;
  esac
done
shift $((OPTIND-1))

setup(){
  echo "Creating $MACVLAN link on $IFNAME"
  ip link add link $IFNAME name $MACVLAN type macvlan mode bridge

  echo "Set ipaddr $IPADDR on $MACVLAN"
  ip addr add "$IPADDR" dev "$MACVLAN"

  echo "Interface UP"
  ip link set dev "$MACVLAN" up

  echo "Replacing original route"
  ROUTE="$(ip route show | grep "$NETWORK" | sed -e "s/$IFNAME/$MACVLAN/")"
  [ -n "$ROUTE" ] && ip route replace $(ip route show | grep "$NETWORK" | sed -e "s/$IFNAME/$MACVLAN/")
}

cleanup(){
  echo "Set original route"
  ROUTE="$(ip route show | grep "$NETWORK" | sed -e "s/$MACVLAN/$IFNAME/")"
  [ -n "$ROUTE" ] && ip route replace $(ip route show | grep "$NETWORK" | sed -e "s/$MACVLAN/$IFNAME/") 

  echo "Interface $MACVLAN DOWN"
  ip link set dev "$MACVLAN" down

  echo "Flushing $MACVLAN ip address"
  ip addr flush "$MACVLAN"

  echo "Removing $MACVLAN"
  ip link del "$MACVLAN"
}

if [[ "$ACTION" == "setup" ]]; then
  echo "Running setup with options: $IFNAME, $MACVLAN, $IPADDR"
  setup

elif [[ "$ACTION" == "cleanup" ]]; then
  echo "Running cleanup with options: $IFNAME, $MACVLAN, $IPADDR"
  cleanup

else
  echo "Undefined behavior"
  exit 1
fi

exit 0

