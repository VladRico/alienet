#!/bin/bash


if [[ $EUID != 0 ]]; then
    echo "Please run as root"
    exit
fi

set +u 

ACTION=setup
BRNAME=br0
IFNAME=enp6s0
TAPNAME=net0

set -ue -o pipefail


usage() {

  echo "Used to create a tuntap network interface, that could be used directly with qemu"
  echo "usage: $0 (-s|-c) -b <bridgeName> -i <interfaceName> -t <tuntapName>" 
  echo ""
  echo "Must be run as root"
  echo ""

  echo -e "-s \t\t\t   setup" 
  echo -e "-c \t\t\t   cleanup" 
  echo -e "-b <bridgeName> \t   name of the bridge (Default: $BRNAME)"
  echo -e "-i <interfaceName> \t   name of the host interface (Default: $IFNAME)"
  echo -e "-t <tuntapName> \t   name of the tuntap interface (Default: $TAPNAME)"
  echo ""

  echo "Example: $0 -s -b $BRNAME -i $IFNAME -t $TAPNAME" 
}

# Parse options
while getopts "i:b:t:sch" opt; do
  case ${opt} in
    i ) IFNAME="$OPTARG";;
    b ) BRNAME="$OPTARG" ;;
    t ) TAPNAME="$OPTARG" ;;
    s ) ACTION="setup" ;;
    c ) ACTION="cleanup" ;;
    h ) usage && exit 0 ;;
    \? )
      echo "Invalid option: -$OPTARG" 1>&2
      exit 1
      ;;
    : )
      echo "Option -$OPTARG requires an argument" 1>&2
      exit 1
      ;;
  esac
done
shift $((OPTIND-1))

setup(){
  echo "Creating bridge device $BRNAME"
  #brctl addbr "$BRNAME"
  ip link add "$BRNAME" type bridge

  echo "Flushing $IFNAME"
  ip addr flush dev "$IFNAME" 

  echo "Adding $IFNAME to $BRNAME"
  #brctl addif "$BRNAME" "$IFNAME"
  ip link set "$IFNAME" master "$BRNAME"

  echo "Creating tap device $TAPNAME"
  ip tuntap add dev "$TAPNAME" mode tap group "$(whoami)"

  echo "Adding $TAPNAME to $BRNAME"
  #brctl addif "$BRNAME" "$TAPNAME"
  ip link set "$TAPNAME" master "$BRNAME"

  echo "Up everything"
  ip link set dev "$BRNAME" up
  ip link set dev "$IFNAME" up
  ip link set dev "$TAPNAME" up

  echo "Showing conf"
  #brctl show

  echo "Assign IP to $BRNAME via dhcp"
  dhclient -v "$BRNAME"

}

cleanup(){

  echo "Removing $TAPNAME from $BRNAME"
  #brctl delif "$BRNAME" "$TAPNAME"
  ip link set "$TAPNAME" nomaster

  echo "Removing $TAPNAME"
  ip tuntap del dev "$TAPNAME" mode tap 

  echo "Removing $IFNAME from $BRNAME"
  #brctl delif "$BRNAME" "$IFNAME"
  ip link set "$IFNAME" nomaster

  echo "Down $BRNAME"
  dhclient -v -r "$BRNAME"
  ip link set dev "$BRNAME" down

  echo "Remove $BRNAME"
  #brctl delbr "$BRNAME"
  ip link del "$BRNAME" type bridge

  echo "Up $IFNAME"
  ip link set dev "$IFNAME" up
  dhclient -v "$IFNAME"
}

if [[ "$ACTION" == "setup" ]]; then
  echo "Running setup with options: $BRNAME, $IFNAME, $TAPNAME"
  setup

elif [[ "$ACTION" == "cleanup" ]]; then
  echo "Running cleanup with options: $BRNAME, $IFNAME, $TAPNAME"
  cleanup

else
  echo "Undefined behavior"
  exit 1
fi

exit 0
