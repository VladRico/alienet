# Lab

Automation using `libvirt` and alpine OS to produce powerful lightweight multi-purpose labs.

## Usage

- Download and install alpine OS to create a template. 
- Configure it as you want. 
- Clone it

## Scripts

Work in Progress --> Use at your own risk

- `copy_vm.sh` is used to clone the template lab to pop new VM
- `macvtap.sh` is a little helper used to automatically setup macvtap interface on the host and routing traffic inside it. Used to communicate with VMs
- `qemu_bridge.sh` is a little helper used to automatically setup bridge interface on host. 
