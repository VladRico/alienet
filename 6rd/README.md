# Complete Router/Firewall dual stack ipv4/ipv6 based on "pure" Debian 10

My ISP offer a public IPV4 address obtain by DHCP over a specific vlan (id 836).  
IPV6 connectivity is made with a specific ip6 in ip4 tunnel call 6rd (for 6 rapid deployment and also because of the name "Remi Després" a french engineer who invented it).   
On the Lan part, I choose radvd for ipv6 Router Advertisement Daemon & Dnsmasq / SNAT for ipv4 stack.  
Nowadays, a lot of things change in Linux with systemd naming interfaces, nftables etc. For better and for worse ...     
It was the good time to verify that recent debian distributions could still do the job as a router and opportunity to implement a complete dual stack router with this new tools & replace the CPE provided by the ISP.  
**Most of important configurations are given on repo with notation: etc_myfile.conf => /etc/myfile.conf**  
dnsmaq configuration is not given as is a very common & an easy part to configure.  
It's easier to deal with your own interfaces name (ie: lan, wan) than with enp0s25 .... given by the "persistent name scheme" use in conjonction with systemd. Also, network interfaces are configurated with systemd/network/interfaces .link files (given in repo).  
Interesting reads for those who want to understand what is being done here:  
 
 - [Debian 10 NetworkInterfaceNames](https://wiki.debian.org/NetworkInterfaceNames)
 - [Debian 10 Nftable introduction](https://wiki.debian.org/nftables)
 - [Official Nftable documentation](https://wiki.nftables.org/wiki-nftables/index.php/Main_Page)
 - [Debian10 man interfaces](https://manpages.debian.org/buster/ifupdown/interfaces.5.en.html)
 - [6rd tunnel rfc5569](https://datatracker.ietf.org/doc/html/rfc5569)
 - [6rd MTU IETF](https://tools.ietf.org/id/draft-foo-v6ops-6rdmtu-01.html)
 - [Debian10 man radvd](https://manpages.debian.org/buster/radvd/radvd.conf.5.en.html)  

Router is a fanless micro PC (amd64), intel celeron J1900 quad core, 4G RAM, 4 x 1 Gig network interfaces and 120 G SSD hardrive bought around 150$.  
The question is why do not use a specific router-firewall LINUX or BSD based (shorewall, pfsense, etc ...) ?  
Well .... for me, this way is easier and more fun. Moreover, capabilites are greatly improved for all the Lan :wink:

```
                                  
                            
                        ISP (FREE / ILIAD)
                               
                 |          | VLAN 802.1Q (ID 836)
                 |          |       +
       IPV6/64   |          |  6IN4 TUN (6RD)
          +      |          |       +
  192.168.1.1/24 |          |  <PUBLIC IPV4>
                 |          |
        +-----LAN+----------+WAN-----+
        |                            |
        |          DEBIAN 10         |
        |                            |
        |          NFTABLE           |
        |                            |
        |          RADVD             |
        |                            |
        |          DNSMASQ           |
        |                            |
        +----------------------------+

```

![Fanless](6rd/fanless.jpg "Fanless minipc")

